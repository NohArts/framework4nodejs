/*
The occipital lobe is divided into several functional visual areas. Each visual 
*area contains a full map of the visual world. Although there are no anatomical 
markers distinguishing these areas (except for the prominent striations in the 
striate cortex), physiologists have used electrode recordings to divide the cortex 
into different functional regions.

The first functional area is the primary visual cortex. It contains a low-level 
description of the local orientation, spatial-frequency and color properties 
within small receptive fields. Primary visual cortex projects to the occipital 
areas of the ventral stream (visual area V2 and visual area V4), and the 
occipital areas of the dorsal stream—visual area V3, visual area MT (V5), and 
the dorsomedial area (DM).

The ventral stream is known for the processing the "what" in vision, while the 
dorsal stream handles the "where/how." This is because the ventral stream 
provides important information for the identification of stimuli that are stored 
in memory. With this information in memory, the dorsal stream is able to focus on 
motor actions in response to the outside stimuli.

Although numerous studies have shown that the two systems are independent and 
structured separately from another, there is also evidence that both are 
essential for successful perception, especially as the stimuli takes on more 
complex forms. For example, a case study using fMRI was done on shape and 
location. The first procedure consisted of location tasks. The second procedure was in a lit-room where participants were shown stimuli on a screen for 600 ms. They found that the two pathways play a role in shape perception even though location processing continues to lie within the dorsal stream [3]

The dorsomedial (DM) is not as thoroughly studied. However, there is some 
evidence that suggests that this stream interacts with other visual areas. 
A case study on monkeys revealed that information from V1 and V2 areas make up 
half the inputs in the DM. The remaining inputs are from multiple sources that 
have to do with any sort of visual processing [4]

A significant functional aspect of the occipital lobe is that it contains the 
primary visual cortex.

Retinal sensors convey stimuli through the optic tracts to the lateral 
geniculate bodies, where optic radiations continue to the visual cortex. Each 
visual cortex receives raw sensory information from the outside half of the 
retina on the same side of the head and from the inside half of the retina on 
the other side of the head. The cuneus (Brodmann's area 17) receives visual 
information from the contralateral superior retina representing the inferior 
visual field. The lingula receives information from the contralateral inferior 
retina representing the superior visual field. The retinal inputs pass through 
a "way station" in the lateral geniculate nucleus of the thalamus before 
projecting to the cortex. Cells on the posterior aspect of the occipital lobes' 
gray matter are arranged as a spatial map of the retinal field. Functional 
neuroimaging reveals similar patterns of response in cortical tissue of the 
lobes when the retinal fields are exposed to a strong pattern.
*/

