/*
The frontal lobe plays a large role in voluntary movement. It houses the 
primary motor cortex which regulates activities like walking.

The function of the frontal lobe involves the ability to project future 
consequences resulting from current actions, the choice between good and bad 
actions (or better and best) (also known as conscience), the override and 
suppression of socially unacceptable responses, and the determination of 
similarities and differences between things or events.

The frontal lobe also plays an important part in integrating longer non-task 
based memories stored across the brain. These are often memories associated 
with emotions derived from input from the brain's limbic system. The frontal 
lobe modifies those emotions to generally fit socially acceptable norms.

Psychological tests that measure frontal lobe function include finger tapping 
(as the frontal lobe controls voluntary movement), the Wisconsin Card Sorting 
Test, and measures of language and numeracy skills.
*/

