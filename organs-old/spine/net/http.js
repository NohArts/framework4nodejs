var dns = require('native-dns');

exports.port = 60080;

exports.events = {
    init: function (config) {
        exports.serv = require('redbird')({
            port: exports.port,
            xfwd: false,
        });

        exports.docker = require('redbird').docker(exports.serv);

        // exports.docker.register("example.com", 'company/myimage:latest');
    },
    open: function (session) {
        // Route to any global ip
        proxy.register("optimalbits.com", "http://167.23.42.67:8000");

        // Route to any local ip, for example from docker containers.
        proxy.register("example.com", "http://172.17.42.1:8001");

        // Route from hostnames as well as paths
        proxy.register("example.com/static", "http://172.17.42.1:8002");
        proxy.register("example.com/media", "http://172.17.42.1:8003");

        // Subdomains, paths, everything just works as expected
        proxy.register("abc.example.com", "http://172.17.42.4:8080");
        proxy.register("abc.example.com/media", "http://172.17.42.5:8080");

        // Route to any href including a target path
        proxy.register("foobar.example.com", "http://172.17.42.6:8080/foobar");

        // You can also enable load balancing by registering the same hostname with different
        // target hosts. The requests will be evenly balanced using a Round-Robin scheme.
        /*
        proxy.register("balance.me", "http://172.17.40.6:8080");
        proxy.register("balance.me", "http://172.17.41.6:8080");
        proxy.register("balance.me", "http://172.17.42.6:8080");
        proxy.register("balance.me", "http://172.17.43.6:8080");
        //*/
    },
    close: function (reason, details) {
        //callbacks.IO.finish(reason, details);
    },
};

exports.program = function (session) {
    console.log("Running HTTP server on port "+exports.port+" ...");

    server.serve(exports.port);
};

exports.methods = {
    'reactor.serve.http.route': function (target, source) {
        exports.serv.register("optimalbits.com", "http://167.23.42.67:8000");
    },
    'reactor.serve.http.contain': function (args) {
        // exports.docker.register("example.com", 'company/myimage:latest');
    },
};

exports.topics = {
    /*
    '<sense>.image': function (args) {
    },
    //*/
};

