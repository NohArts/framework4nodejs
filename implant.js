var common = require('./common.js');

var args = [];

if (process.argv.length < 2) {
        console.log("Must supply an organ's type !")
} else {
    args.push(process.argv[2]);

    if (process.argv.length < 3) {
        console.log("Must supply an organ's name !")
    } else {
        args.push(process.argv[3]);

        if (process.argv.length < 4) {
            args.push('default');
        } else {
            args.push(process.argv[4]);
        }
    }
}

switch (args[0]) {
    case 'list':
        console.log("Cortex :");
        for (var i=0 ; i<common.anatomy.cortex.length ; i++) {
            console.log("\t-> ",common.anatomy.cortex[i]);
        }

        console.log("Plug-in :");
        for (var i=0 ; i<common.anatomy.plugin.length ; i++) {
            console.log("\t-> ",common.anatomy.plugin[i]);
        }

        console.log("Profile :");
        for (var i=0 ; i<common.profile.length ; i++) {
            console.log("\t-> ",common.profile[i]);
        }
        break;

    case 'exec':
        common.kernel(args[1], args[2], args[3], function () {
            common.program(function (session) {
                console.log("#) Running implant successfully ...")
            });
        });
        break;

    case 'help':
    default:
        break;
}
