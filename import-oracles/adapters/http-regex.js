var semver = require('semver');
var _ = require('lodash');
var agent = require('superagent');

var NOOP = function() {};

function HttpSource(options) {
  _.extend(this, {
    time: options.time,
    expr: options.expr,
    name: options.name,
    url: options.link,
    all: [],
    stable: [],
    updated: undefined
  }, options);
}

HttpSource.prototype.update = function(done) {
  done = done || NOOP;

  return agent.get(this.url)
    .timeout(this.time)
    .end(parseResponse.bind(this));

  function parseResponse(err, res) {
    if (err) {
      return done(err, false);
    }
    if (!res.text) {
      return done(new Error('No response'), false);
    }
    if (res.status !== 200) {
      return done(new Error('Bad response'), false);
    }

    this._parse(res.text);

    return done(undefined, true);
  }
};

HttpSource.prototype._parse = function(body) {
  var matches = body.match(this.expr);
  var versions = _.unique(matches);

  this.all = versions.sort(semver.compare);
  this.stable = versions.filter(function(version) {
    return semver(version).prerelease.length === 0;
  });
  this.updated = new Date();
};

module.exports = HttpSource;

