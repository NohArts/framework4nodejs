var semver = require('semver');
var _ = require('lodash');
var NpmStats = require('npm-stats');

var NOOP = function() {};

module.exports = NpmStatsSource;

function NpmStatsSource(options) {
  _.extend(this, {
    timeout: options.time,
    narrow: options.page,
    name: options.name,
    registry: options.book,
    all: [],
    stable: [],
    updated: undefined
  }, options);
}

NpmStatsSource.prototype.update = function(done) {
  done = done || NOOP;

  NpmStats(this.registry)
    .module(this.narrow)
    .info(parseResponse.bind(this));

  function parseResponse(err, info, response) {
    if (err) return done(err, false);
    if (response.statusCode !== 200) return done(new Error('Bad response'), false);

    this._parse(info);
    done(undefined, true);
  }
};

NpmStatsSource.prototype._parse = function(info) {
  var versions = _.unique(Object.keys(info.versions));
  var tags = _.unique(Object.keys(info['dist-tags']));  // omitting as this breaks semver, needs a 'tags' concept
  var latestStable = info['dist-tags'].latest;

  this.all = versions.filter(semver.valid).sort(semver.compare);
  this.stable = this.all.filter(semver.gte.bind(semver, latestStable));
  this.updated = new Date();
};

