module.exports = function (cfg,app) {
    app.get('/board/', function (req,res) {
        app.render('views/home');
    });

    return app;

    app.get('/', function (req, res) {
        res.render('index', {});
    });

    app.get('/people', function (req, res) {
        res.render('people', { people: people });
    });

    app.get('/people/:id', function (req, res) {
        res.render('person', { person: people[req.params.id] });
    });

    app.get('/*', function (req, res) {
        res.render(req.params[0], {});
    });
};

