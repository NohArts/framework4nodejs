var _ = require('lodash');
var logfmt = require('logfmt');
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');

var router = require('./router');
var render = require('./render');
var Resolver = require('./resolver');

var NodeSource = require('./sources/node');
var IoJsSource = require('./sources/iojs');
var NpmSource = require('./sources/npm');
var YarnSource = require('./sources/yarn');
var NginxSource = require('./sources/nginx');
var MongoDBSource = require('./sources/mongodb');

var Dashboard = require('./dashboard');

module.exports = function App(cfg) {
  var app = express();
  var resolvers = cfg.resolvers || {
    node: new Resolver(new NodeSource()),
    iojs: new Resolver(new IoJsSource()),
    npm: new Resolver(new NpmSource()),
    nginx: new Resolver(new NginxSource()),
    mongodb: new Resolver(new MongoDBSource()),
    yarn: new Resolver(new YarnSource())
  };

  app.resolvers = resolvers;

  if (process.env.NODE_ENV !== 'test') {
    app.use(logfmt.requestLogger());
  }

  var middlewares = cfg.pipelines || [];

  app
    .use(cors())
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({ extended: true }))
    .use('/cdn/vendor', express.static('../node_modules/gentelella/vendors'))
    .use('/cdn/assets', express.static('../node_modules/gentelella/build'))
    .get('/doc', renderInstructions)
    .get('/ssl', sendSSL);

  for (var i=0 ; i<middlewares.length ; i++) {
    app.use(middlewares[i]);
  }

  Object.keys(resolvers).forEach(function attachRouter(key) {
    app.use('/' + key + ':format?', router(resolvers[key]));
  });

  return Dashboard(cfg,app);

  function renderInstructions(req, res, next) {
    render(resolvers, onRender);

    function onRender(err, html) {
      if (err) throw err;
      res.send(html);
    }
  }

  function sendSSL(req, res, next) {
    res.type('text');
    res.send([
      '"Demonstration of domain control for DigiCert order #00462258"',
      '"Please send the approval email to: ops@heroku.com"'
    ].join('\n'));
  }
};
