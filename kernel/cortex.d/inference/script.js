var RuleEngine = require('node-rules');

exports.engine = {};

exports.events = {
    init: function (config) {

    },
    open: function (session) {

    },
    close: function (reason, details) {

    },
};

exports.methods = {
    '<self>.rules.process': function (args) {
        var R = new RuleEngine();

        for (var i=0 ; i<args[0].length ; i++) {
            R.register(args[0][i]);
        }

        R.execute(args[1], function(data) {
            return data;

            console.log("Finished with value", data.someval);
        });
    },
    '<self>.rules.export': function (args) {
        var R = new RuleEngine();

        for (var i=0 ; i<args[0].length ; i++) {
            R.register(args[0][i]);
        }

        return R.toJSON();
    },
};

exports.topics = {
    /*
    '<self>.image': function (args) {
        
    },
    //*/
};

exports.program = function (session) {
    exports.invoke('<self>.rules.process', [
        [{
            "condition": function(R) {
                R.when(this.someval < 10);
            },
            "consequence": function(R) {
                console.log(++this.someval, " : incrementing again till 10");
                R.restart();
            }
        },{
            "name": "transaction minimum",
            "priority": 3,
            "on" : true,
            "condition": function(R) {
                R.when(this.transactionTotal < 500);
            },
            "consequence": function(R) {
                this.result = false;
                R.stop();
            }
        }],
        {
            "someval": 0
        }
    ]).then(function (data) {
        console.log("Finished with value", data.someval);
    });

    /**************************************************************************/
    /**************************************************************************/

    exports.invoke('<self>.rules.process', [
        [{
            "condition": function(R) {
                R.when(this.application === "MOB");
            },
            "consequence": function(R) {
                this.isMobile = true;
                R.next();//we just set a value on to fact, now lests process rest of rules
            }
        }, {
            "condition": function(R) {
                R.when(this.cardType === "Debit");
            },
            "consequence": function(R) {
                this.result = false;
                this.reason = "The transaction was blocked as debit cards are not allowed";
                R.stop();
            }
        }],
        {
            "name": "user4",
            "application": "MOB",
            "transactionTotal": 600,
            "cardType": "Credit"
        }
    ]).then(function (data) {
        if (data.result) {
            console.log("Valid transaction");
        } else {
            console.log("Blocked Reason:" + data.reason);
        }

        if(data.isMobile) {
            console.log("It was from a mobile device too!!");
        }
    });
};

