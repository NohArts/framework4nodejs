var path = require('path');
var http = require('http');
var express = require("express");

var RED = require("node-red");
var solid = require("solid-server");
var ParseServer = require('parse-server').ParseServer;

var cfg = {
    root: 'temp/static',
    tool: 'temp/ww-root',
    port: process.env.PORT || 8000,
    path: {
        red:   '/flow',
        api:   '/red',
        mongo: '/parse',
    },
    link: [
        { mount: '/profile', path: process.env.APP_HOME },
        { mount: '/storage', path: process.env.APP_DIRS },
    ],
    mongo: process.env.DATABASE_URI || process.env.MONGODB_URI,
};

function solidify (app,entry) {
    var srv = solid({
        cache: 0, // Set cache time (in seconds), 0 for no cache
        live: true, // Enable live support through WebSockets
        root: entry.path, // Root location on the filesystem to serve resources
        secret: 'node-ldp', // Express Session secret key
        cert: false, // Path to the ssl cert
        key: false, // Path to the ssl key
        mount: entry.mount, // Where to mount Linked Data Platform
        webid: false, // Enable WebID+TLS authentication
        suffixAcl: '.acl', // Suffix for acl files
        proxy: false, // Where to mount the proxy
        errorHandler: false, // function(err, req, res, next) to have a custom error handler
        errorPages: false, // specify a path where the error pages are
        fileBrowser: 'http://localhost:'+cfg.port+'/tools/warp/#/list/',
    });

    try {
        app.use(entry.mount,srv);

        return true;
    } catch (ex) {
        // console.log("\t\thttp://localhost:" + cfg.port + cfg.link[i].mount + " ...");
    }

    return false;
}

/******************************************************************************/

if (!cfg.mongo) {
    console.log('DATABASE_URI not specified, falling back to localhost.');
}

var srv = {
    xprs:  express(),
    mongo: new ParseServer({
        databaseURI: cfg.mongo || 'mongodb://localhost:27017/dev',
        cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
        appId: process.env.APP_ID || 'myAppId',
        masterKey: process.env.MASTER_KEY || '', //Add your master key here. Keep it secret!
        serverURL: process.env.SERVER_URL || 'http://localhost:1337/parse',  // Don't forget to change to https if needed
        liveQuery: {
            classNames: ["Posts", "Comments"] // List of classes to support for query subscriptions
        }
    });
};

srv.http = http.createServer(srv.xprs);

srv.xprs.use("/static", express.static(cfg.root));
srv.xprs.use("/tools",  express.static(cfg.tool));

/******************************************************************************/

app.use(cfg.path.mongo, srv.mongo);

// Parse Server plays nicely with the rest of your web routes
app.get('/', function(req, res) {
  res.status(200).send('I dream of being a website.  Please star the parse-server repo on GitHub!');
});

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', function(req, res) {
  res.sendFile(path.join(__dirname, '/public/test.html'));
});

/******************************************************************************/

RED.init(srv.http, {
    httpAdminRoot: cfg.path.red,
    httpNodeRoot:  cfg.path.api,
    userDir: process.env.APP_EXEC+"/red/",
    functionGlobalContext: { }    // enables global context
});

console.log("\t-> Mounting Node-RED Editor UI on : http://localhost:" + cfg.port + cfg.path.red);
srv.xprs.use(cfg.path.red,RED.httpAdmin);

console.log("\t-> Mounting Node-RED Nodes UI on : http://localhost:" + cfg.port + cfg.path.api);
srv.xprs.use(cfg.path.api,RED.httpNode);

/******************************************************************************/

console.log("\t-> Mounting Solid on :");

for (var i=0 ; i<cfg.link.length ; i++) {
    if (solidify(srv.xprs, cfg.link[i])==true) {
        console.log("\t\thttp://localhost:" + cfg.port + cfg.link[i].mount + " ...");
    }
}

/******************************************************************************/

console.log("\t-> Listening on 0.0.0.0:"+cfg.port+" ...");
srv.http.listen(cfg.port, function () {
    console.log('#> Running on port '+cfg.port+' ...');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(srv.http);

console.log("\t-> Start Node-RED runtime ...");
//RED.start();

