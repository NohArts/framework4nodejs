const _ = require('lodash');
const path = require('path');

var express = require('express');
var neo4j = require('node-neo4j'); // there we are importing dependencies

const cgkb = require(path.join(__dirname, 'lib', 'cgkb'));

/************************************************************************/

var neo4j_url = process.env.GRAPH_URL;

neo4j_url = neo4j_url.replace('/browser/','')
neo4j_url = neo4j_url.replace('/db/data/','')

var db = new neo4j(neo4j_url); // there you will provide your neo4j host url

var app = express();
app.enable('trust proxy');

app.set('views',__dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

/************************************************************************/

    app.post('/user', function (req, res) {
            db.insertNode({
                name: 'Prabjot',
                sex: 'male'
            },function(err, node){
             if(err) throw err;
                 // Output node properties.
                    console.log(node.data);
                 // Output node id.
                    console.log(node._id);
            });
    });
 
    app.get('/user', function (req, res) {
        db.readNode(398, function(err, node){
            if(err){
                console.log("err is"+err);
                throw err;
            }
            // Output node properties.
            console.log(node);
            res.render('index.html',{node:node}); // so there is cool
        feature of ejs,we set response in variable “node” and we            can access it on html as like that (<%= node %>)
        });
    });
    app.put('/userUpdate', function (req, res) {
        db.updateNode(398, {name:'Prabjot Singh'}, function(err, node){
            if(err) throw err;
            if(node === true){
                // node updated
            } else {
                // node not found, hence not updated
            }
        });
    });
    app.delete('/userDelete', function (req, res) {
        db.deleteNode(398, function(err, node){
if(err) throw err;
            if(node === true){
                // node deleted
            } else {
                // node not deleted because not found or because of existing relationships
            }
        });
    });

/************************************************************************/

var httpServer = require('http').createServer(app);
httpServer.listen(cfg.port, function() {
  console.log('parse-dashboard running on port ' + cfg.port + '.');
});

